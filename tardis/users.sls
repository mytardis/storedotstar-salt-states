mytardis-group:
  group:
    - name: mytardis
    - present

mytardis-user:
  user:
    - name: mytardis
    - present
    - shell: /bin/bash
    - groups:
      - mytardis
    - home: /home/mytardis
    - require:
      - group: mytardis-group
