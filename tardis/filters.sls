filters-pkgs:
  pkg:
    - installed
    - names:
        - gfortran
        - gnumeric
        - imagemagick
        - libatlas-base-dev
        - libblas-dev
        - liblapack-dev
        - libopenblas-dev
        - {{ 'openjdk-7-jdk' if grains['osfinger'] == 'Ubuntu-14.04' else 'openjdk-8-jdk' }}

numpy:
  pip.installed:
    - name: numpy >= 1.9
    - bin_env: /home/mytardis/virtualenvs/mytardis
    - ignore_installed: true
    - force_reinstall: false
    - user: mytardis

bioformats:
  pip.installed:
    - editable: git+https://github.com/mytardis/mytardisbf.git@master#egg=mytardisbf
    - bin_env: /home/mytardis/virtualenvs/mytardis
    - ignore_installed: true
    - force_reinstall: false
    - user: mytardis
    - require:
        - virtualenv: mytardis-venv
        - pip: numpy
        - pip: python-bioformats
        - pip: javabridge

javabridge:
  pip.installed:
    - name: javabridge == 1.0.14
    - bin_env: /home/mytardis/virtualenvs/mytardis
    - ignore_installed: true
    - force_reinstall: false
    - user: mytardis

python-bioformats:
  pip.installed:
    - name: git+https://github.com/mytardis/python-bioformats.git@mytardis#egg=python-bioformats
    - bin_env: /home/mytardis/virtualenvs/mytardis
    - ignore_installed: true
    - force_reinstall: false
    - user: mytardis
    - require:
        - virtualenv: mytardis-venv
        - pip: javabridge
        - pip: numpy

r-base:
  pkg.installed:
    - pkgs:
      - r-base
    - fromrepo: trusty
    - version: 3.4.0-1trusty0

CRAN:
  pkgrepo.managed:
    - humanname: Comprehensive R Archive Network
    - name: "deb https://cran.csiro.au/bin/linux/ubuntu trusty/"
    - file: /etc/apt/sources.list.d/r-project.list
    - keyid: E084DAB9
    - keyserver: keyserver.ubuntu.com
    - require_in:
      - pkg: r-base

Bioconductor:
  cmd.script:
    - source: salt://tardis/templates/install_bioconductor
    - cwd: /root/
    - user: root

rpy2:
  pip.installed:
    - name: rpy2 == 2.7.8
    - bin_env: /home/mytardis/virtualenvs/mytardis
    - ignore_installed: true
    - force_reinstall: false
    - user: mytardis
    - require:
        - virtualenv: mytardis-venv
        - pkg: r-base

/home/mytardis/mytardis/tardis/tardis_portal/filters/fcs:
  file:
    - directory
    - user: mytardis
    - group: mytardis
    - require:
      - git: mytardis-git

fcs-filter:
  git:
    - name: https://github.com/wettenhj/fcs-mytardis-filter.git
    - latest
    - target: /home/mytardis/mytardis/tardis/tardis_portal/filters/fcs
    - force_clone: true
    - force_checkout: true
    - force_reset: true
    - require:
      - file: /home/mytardis/mytardis/tardis/tardis_portal/filters/fcs

/home/mytardis/mytardis/tardis/tardis_portal/filters/pdf:
  file:
    - directory
    - user: mytardis
    - group: mytardis
    - require:
      - git: mytardis-git

pdf-filter:
  git:
    - name: https://github.com/wettenhj/pdf-mytardis-filter.git
    - latest
    - target: /home/mytardis/mytardis/tardis/tardis_portal/filters/pdf
    - force_clone: true
    - force_checkout: true
    - force_reset: true
    - require:
      - file: /home/mytardis/mytardis/tardis/tardis_portal/filters/pdf

/home/mytardis/mytardis/tardis/tardis_portal/filters/xlsx:
  file:
    - directory
    - user: mytardis
    - group: mytardis
    - require:
      - git: mytardis-git

xlsx-filter:
  git:
    - name: https://github.com/wettenhj/xlsx-mytardis-filter.git
    - latest
    - target: /home/mytardis/mytardis/tardis/tardis_portal/filters/xlsx
    - force_clone: true
    - force_checkout: true
    - force_reset: true
    - require:
      - file: /home/mytardis/mytardis/tardis/tardis_portal/filters/xlsx

/home/mytardis/mytardis/tardis/tardis_portal/filters/csv:
  file:
    - directory
    - user: mytardis
    - group: mytardis
    - require:
      - git: mytardis-git

csv-filter:
  git:
    - name: https://github.com/wettenhj/csv-mytardis-filter.git
    - latest
    - target: /home/mytardis/mytardis/tardis/tardis_portal/filters/csv
    - force_clone: true
    - force_checkout: true
    - force_reset: true
    - require:
      - file: /home/mytardis/mytardis/tardis/tardis_portal/filters/csv
