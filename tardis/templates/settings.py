import json
import os
import urllib
from datetime import timedelta

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

from .default_settings import *

# Load the json settings file
json_settings_path = os.path.join(
    os.path.dirname(
        os.path.realpath(__file__)),
    'settings.json')
with open(json_settings_path) as pillar_data_file:
    pillar = json.load(pillar_data_file)['settings']

# Turn on django debug mode.
DEBUG = {{ debug }}

# Database settings
DATABASES['default']['ENGINE'] = 'django.db.backends.postgresql_psycopg2'
DATABASES['default']['NAME'] = '{{ tardis_db }}'
DATABASES['default']['USER'] = '{{ tardis_db_user }}'
DATABASES['default']['PASSWORD'] = '{{ tardis_db_password }}'
DATABASES['default']['HOST'] = '{{ tardis_db_host }}'
DATABASES['default']['PORT'] = '5432'

SECRET_KEY = pillar['secret_key']

STATIC_ROOT = '/mnt/static_files'

CELERY_RESULT_BACKEND = 'amqp'
BROKER_URL = 'amqp://%(user)s:%(pw)s@%(ipaddrs)s:5672/%(vhost)s' % {
    'user': '{{ rabbitmq_user }}',
    'pw': urllib.quote_plus('{{ rabbitmq_password }}'),
    'ipaddrs': '{{ rabbitmq_ip }}',
    'vhost': '{{ rabbitmq_vhost }}'
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'console': {
            'format': '%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        },
        'django': {
            'format': 'django: %(message)s',
        },
    },

    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'console',
        },
        'logging.handlers.SysLogHandler': {
            'level': 'ERROR',
            'class': 'logging.handlers.SysLogHandler',
            'facility': 'local7',
            'formatter': 'django',
            'address': '/dev/log',
        },
    },

    'loggers': {
        '': {
            'handlers': ['logging.handlers.SysLogHandler'],
            'propagate': True,
            'format': 'django: %(message)s',
            'level': 'ERROR',
        },
        # Redefining the logger for the `django` module
        # prevents invoking the `AdminEmailHandler`
        'django': {
            'handlers': ['console'],
            'level': 'INFO'
        }
    }
}

# Authn/Authz
# Set auth and group providers if specified in pillar:
if 'auth_providers' in pillar:
    AUTH_PROVIDERS = tuple(pillar.get('auth_providers'))
if 'group_providers' in pillar:
    GROUP_PROVIDERS = tuple(pillar.get('group_providers'))

# Set authentication backends if specified in pillar:
if 'authentication_backends' in pillar:
    AUTHENTICATION_BACKENDS = tuple(pillar.get('authentication_backends'))

# Context processors
TEMPLATES[0]['OPTIONS']['context_processors'].extend(
    pillar.get('context_processors', []))

# User menu modifiers
USER_MENU_MODIFIERS.extend(
    pillar.get('user_menu_modifiers', []))

# Overridable login views
if 'login_views' in pillar:
    LOGIN_VIEWS_PILLAR = pillar.get('login_views')
    LOGIN_VIEWS = { int(key): LOGIN_VIEWS_PILLAR[key] for key in LOGIN_VIEWS_PILLAR.keys() }

# LDAP configuration
LDAP_USE_TLS = pillar.get('ldap_use_tls', False)
LDAP_URL = str(pillar.get('ldap_url', ''))

LDAP_USER_LOGIN_ATTR = str(pillar.get('ldap_user_login_attr', ''))
LDAP_USER_ATTR_MAP = {str(key): str(value) for key, value in
                      pillar.get('ldap_user_attr_map', {}).items()}
LDAP_GROUP_ID_ATTR = str(pillar.get('ldap_group_id_attr', ''))
LDAP_GROUP_ATTR_MAP = {str(key): str(value) for key, value in
                       pillar.get('ldap_group_attr_map', {}).items()}

LDAP_ADMIN_USER = str(pillar.get('ldap_admin_user', ''))
LDAP_ADMIN_PASSWORD = str(pillar.get('ldap_admin_password', ''))
LDAP_BASE = str(pillar.get('ldap_base', ''))
LDAP_USER_BASE = str(pillar.get('ldap_user_base', ''))
LDAP_GROUP_BASE = str(pillar.get('ldap_group_base', ''))

# Set middleware if specified in pillar:
if 'middleware' in pillar:
    MIDDLEWARE = tuple(pillar.get('middleware'))

CELERYBEAT_SCHEDULE = {}
for name, params in pillar.get('celerybeat_schedule', {}).iteritems():
    CELERYBEAT_SCHEDULE[name] = {
        'task': params['task'],
        'schedule': timedelta(
            days=params['schedule'].get('days', 0),
            seconds=params['schedule'].get('seconds', 0),
            microseconds=params['schedule'].get('microseconds', 0),
            milliseconds=params['schedule'].get('milliseconds', 0),
            minutes=params['schedule'].get('minutes', 0),
            hours=params['schedule'].get('hours', 0),
            weeks=params['schedule'].get('weeks', 0)
        ),
        'kwargs': params.get('kwargs', {'priority': DEFAULT_TASK_PRIORITY})
    }

# Enable selected apps
for app in pillar.get('enabled_apps', []):
    INSTALLED_APPS += tuple([app])

# Disable selected apps
for app in pillar.get('disabled_apps', []):
    INSTALLED_APPS = filter(lambda a: a != app, INSTALLED_APPS)

{% if elasticsearch_host %}
# Elasticsearch setup
# Settings for the single search box
SINGLE_SEARCH_ENABLED = True
if SINGLE_SEARCH_ENABLED:
    HAYSTACK_CONNECTIONS = {
        'default': {
            'ENGINE': 'haystack.backends.elasticsearch_backend.'
            'ElasticsearchSearchEngine',
            'URL': 'http://{{ elasticsearch_host }}:9200/',
            'INDEX_NAME': 'haystack',
            'TIMEOUT': 60 * 5,
        },
    }
    INSTALLED_APPS = INSTALLED_APPS + ('haystack',)
else:
    HAYSTACK_CONNECTIONS = {
        'default': {
            'ENGINE': 'haystack.backends.simple_backend.SimpleEngine',
        },
    }
HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'
{% endif %}

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
DEFAULT_ARCHIVE_FORMATS = ['tar']
REDIS_VERIFY_MANAGER = False

# Clear a few global vars
def del_if_set(name):
    if name in globals():
        del(globals()[name])
del_if_set('EMAIL_PORT')
del_if_set('EMAIL_HOST')
del_if_set('EMAIL_HOST_USER')
del_if_set('EMAIL_HOST_PASSWORD')
del_if_set('EMAIL_USE_TLS')

# Add arbitrary string attributes as defined in the pillar
for name, value in pillar.get('extra_parameters', {}).iteritems():
    globals()[name] = value

if 'ADMINS' in globals():
    MANAGERS = ADMINS

if 'GOOGLE_ANALYTICS_ID' in globals():
    TEMPLATES[0]['OPTIONS']['context_processors'] += [
        'tardis.tardis_portal.context_processors.google_analytics']

CUSTOM_ABOUT_SECTION_TEMPLATE = os.path.join('tardis_portal', pillar.get(
    'custom_about_page_template', 'about_include.html'))

if 'sentry_dsn' in pillar:
    sentry_sdk.init(
        dsn=pillar.get('sentry_dsn'),
        integrations=[DjangoIntegration()]
    )
