#!/bin/bash
iostat -y 1 1 | grep -A1 avg-cpu | tail -n 1 | awk '{print $4}'
