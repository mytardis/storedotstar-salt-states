{% set codename = grains.get('lsb_distrib_codename')|lower %}

repository_pgdg:
  pkgrepo.managed:
    - humanname: {{ codename }}-backports
    - file: /etc/apt/sources.list.d/pgdg.list
    - refresh_db: True
    - clean_file: True
    - name: deb http://apt.postgresql.org/pub/repos/apt/ {{ codename }}-pgdg main
    - key_url: https://www.postgresql.org/media/keys/ACCC4CF8.asc

postgresql-client-9.3:
  pkg.installed:
  - name: postgresql-client-9.3
  - require:
    - pkgrepo: repository_pgdg
