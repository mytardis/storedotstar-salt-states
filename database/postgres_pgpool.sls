# Installs pgpool to manage a postgresql cluster in streaming replication mode

include:
  - database.postgres_keys

# Installing pgpool2 from the Postgres repository we are using to install
# Postgres 9.3 on Ubuntu 16 would give us pgpool2 version 4.0.2 which our
# pgprepmgr.sh script is not compatible with, so for now, we will install
# the Ubuntu package for pgpool2, rather than the one from apt.postgresql.org

pgpool2:
  pkg.installed:
  - pkgs:
    {% if grains['osfinger'] == 'Ubuntu-14.04' %}
    - pgpool2: 3.3.2-1ubuntu1
    - libpgpool0: 3.3.2-1ubuntu1
    {% else %}
    - pgpool2: 3.4.3-1
    - libpgpool0: 3.4.3-1
    {% endif %}
  service.running:
    - watch:
        - file: pgpool2_config
        - file: pgpool2_hba_config

delete_original_pcp_config:
  cmd.wait:
    - name: rm /etc/pgpool2/pcp.conf
    - watch:
        - pkg: pgpool2

update_pcp:
  cmd.run:
    - name: echo "{{ salt['pillar.get']('pcp_credentials:user', 'postgres') }}:$(pg_md5 {{ salt['pillar.get']('pcp_credentials:password', 'change_me') }})" > /etc/pgpool2/pcp.conf
    - creates: /etc/pgpool2/pcp.conf
    - require:
        - pkg: pgpool2

pgpool2_config:
  file:
    - name: /etc/pgpool2/pgpool.conf
    - managed
    - source: salt://database/templates/pgpool.conf
    - mode: 640
    - template: jinja
    - defaults:
        postgres_user: {{ salt['pillar.get']('postgresql_admin_credentials:user', 'postgres') }}
        postgres_password: {{ salt['pillar.get']('postgresql_admin_credentials:password', '') }}
    - require:
        - pkg: pgpool2

pgpool2_hba_config:
  file:
    - name: /etc/pgpool2/pool_hba.conf
    - append
    - text: 'hostssl    all    all    0.0.0.0/0    trust'
    - require:
        - pkg: pgpool2

failover_script:
  file:
    - name: /var/lib/postgresql/failover_stream.sh
    - managed
    - source: salt://database/templates/failover_stream.sh
    - mode: 755

helper_utility:
  file:
    - name: /home/ubuntu/pgprepmgr.sh
    - managed
    - source: salt://database/templates/pgprepmgr.sh
    - mode: 750
    - user: ubuntu
    - group: root
    - template: jinja
    - defaults:
        pcp_admin_user: {{ salt['pillar.get']('pcp_credentials:user', 'postgres') }}
        pcp_admin_password: {{ salt['pillar.get']('pcp_credentials:password', 'change_me') }}

pgpass:
  file:
    - name: /home/ubuntu/.pgpass
    - managed
    - source: salt://database/templates/pgpass
    - mode: 600
    - user: ubuntu
    - group: root
    - template: jinja

wait_for_pool:
  cmd.run:
    - name: sleep 10
    - order: last
