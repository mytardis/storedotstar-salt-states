
{% if 'rabbitmq' in salt['grains.get']('roles', []) %}
{% set rabbitmq_admin_user = pillar['rabbitmq']['admin_user'] %}
{% set rabbitmq_admin_password = pillar['rabbitmq']['admin_password'] %}
{% set rabbitmq_vhost = pillar['rabbitmq']['vhost'] %}
{% set rabbitmq_ip = salt['mine.get']('G@roles:rabbitmq and G@deployment:'+grains['deployment'], 'network.ipaddrs', expr_form='compound').items()[0][1][0] %}
{% endif %}

{% if 'postgresql_pgpool' in salt['grains.get']('roles', []) %}
{% set pcp_admin_user = salt['pillar.get']('pcp_credentials:user', 'postgres') %}
{% set pcp_admin_password = salt['pillar.get']('pcp_credentials:password', '') %}
{% endif %}

{% set nagios_host = salt['mine.get']('G@roles:nagios_core', 'network.ipaddrs', expr_form='compound').items()[0][1][0] %}

/etc/nagios/nrpe.cfg:
  file.managed:
    - source: salt://nagios/templates/nrpe.cfg
    - template: jinja
    - makedirs: True
    - mode: 644
    - user: root
    - group: root
    - defaults:
        nagios_host: {{ nagios_host }}
    {% if 'rabbitmq' in salt['grains.get']('roles', []) %}
        rabbitmq_admin_user: {{ rabbitmq_admin_user }}
        rabbitmq_admin_password: {{ rabbitmq_admin_password }}
        rabbitmq_vhost: {{ rabbitmq_vhost }}
        rabbitmq_ip: {{ rabbitmq_ip }}
    {% endif %}
    {% if 'celerybeat' in salt['grains.get']('roles', []) %}
        celerybeat_supervisor_process: celerybeat
    {% endif %}
    {% if 'postgresql_pgpool' in salt['grains.get']('roles', []) %}
        pcp_admin_user: {{ pcp_admin_user }}
        pcp_admin_password: {{ pcp_admin_password }}
    {% endif %}

nagios-nrpe-server:
  pkg:
    - installed
  service:
    - running
    - watch:
        - file: /etc/nagios/nrpe.cfg

nagios-plugins:
  pkg:
    - installed

nagios-nrpe-plugin:
  pkg:
    - installed

/usr/lib/nagios/plugins/check_await:
  file.managed:
    - source: salt://nagios/templates/plugins/check_await
    - makedirs: True
    - mode: 755
    - user: root
    - group: root

{% if 'rabbitmq' in salt['grains.get']('roles', []) %}
/usr/lib/nagios/plugins/check_rabbitmq_celery_queue_length:
  file.managed:
    - source: salt://nagios/templates/plugins/check_rabbitmq_celery_queue_length
    - template: jinja
    - makedirs: True
    - mode: 755
    - user: root
    - group: root
    - defaults:
        rabbitmq_vhost: {{ rabbitmq_vhost }}

/usr/lib/nagios/plugins/check_rabbitmq_filters_queue_length:
  file.managed:
    - source: salt://nagios/templates/plugins/check_rabbitmq_filters_queue_length
    - template: jinja
    - makedirs: True
    - mode: 755
    - user: root
    - group: root
    - defaults:
        rabbitmq_vhost: {{ rabbitmq_vhost }}
{% endif %}

{% if 'celerybeat' in salt['grains.get']('roles', []) %}
/usr/lib/nagios/plugins/check_supv.py:
  file.managed:
    - source: salt://nagios/templates/plugins/check_supv.py
    - template: jinja
    - makedirs: True
    - mode: 755
    - user: root
    - group: root
{% endif %}

{% if 'postgresql_pgpool' in salt['grains.get']('roles', []) %}
/usr/lib/nagios/plugins/check_pgpool2:
  file.managed:
    - source: salt://nagios/templates/plugins/check_pgpool2
    - template: jinja
    - makedirs: True
    - mode: 755
    - user: root
    - group: root
{% endif %}

/usr/lib/nagios/plugins/check_mountpoints:
  file.managed:
    - source: salt://nagios/templates/plugins/check_mountpoints
    - makedirs: True
    - mode: 755
    - user: root
    - group: root

/usr/lib/nagios/plugins/check_cifs_kernel_messages:
  file.managed:
    - source: salt://nagios/templates/plugins/check_cifs_kernel_messages
    - makedirs: True
    - mode: 755
    - user: root
    - group: root
