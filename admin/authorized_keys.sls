/home/ubuntu/.ssh/authorized_keys:
  file.managed:
    - user: ubuntu
    - group: ubuntu
    - mode: 600
    - contents_pillar: admin:keys
