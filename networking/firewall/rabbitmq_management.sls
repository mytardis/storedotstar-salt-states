{% if salt['pillar.get']('networking:trusted_ips', None) %}
{% for trusted_ip_source in salt['pillar.get']('networking:trusted_ips', []) %}
rabbitmq_management_{{ trusted_ip_source }}:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: NEW
    - dport: 15671
    - proto: tcp
    - family: ipv4
    - source: {{ trusted_ip_source }}
    - save: True
    - require:
      - iptables: clean_iptables_ipv4
{% endfor %}
{% else %}
rabbitmq_management:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: NEW
    - dport: 15671
    - proto: tcp
    - save: True
    - require:
      - iptables: clean_iptables_ipv4
{% endif %}
